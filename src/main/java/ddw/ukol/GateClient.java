package ddw.ukol;

import gate.Annotation;
import gate.AnnotationSet;
import gate.Corpus;
import gate.CreoleRegister;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.Gate;
import gate.Node;
import gate.ProcessingResource;
import gate.creole.SerialAnalyserController;
import gate.util.GateException;
import gate.util.InvalidOffsetException;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import org.jsoup.Jsoup;
import java.net.URL;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author Vaclav Divis
 */
public class GateClient {

    private static SerialAnalyserController annotationPipeline = null;
    // whether the GATE is initialised
    private static boolean isGateInitilised = false;

    public String run(String str) throws MalformedURLException, IOException {

	if (!isGateInitilised) {

	    // initialise GATE
	    initialiseGate();
	}

	try {
	    // create an instance of a Document Reset processing resource
	    ProcessingResource documentResetPR = (ProcessingResource) Factory.createResource("gate.creole.annotdelete.AnnotationDeletePR");

	    // create an instance of a English Tokeniser processing resource
	    ProcessingResource tokenizerPR = (ProcessingResource) Factory.createResource("gate.creole.tokeniser.DefaultTokeniser");

	    // create an instance of a Sentence Splitter processing resource
	    ProcessingResource sentenceSplitterPR = (ProcessingResource) Factory.createResource("gate.creole.splitter.SentenceSplitter");

	    // create an instance of a Sentence Splitter processing resource
	    ProcessingResource annieGazetteer = (ProcessingResource) Factory.createResource("gate.creole.gazetteer.DefaultGazetteer");

	    // locate the JAPE grammar file
	    File japeOrigFile = new File("C:\\Users\\MUDR\\Desktop\\skola\\DDW\\ukol\\src\\main\\java\\ddw\\ukol\\review.jape");
	    java.net.URI japeURI = japeOrigFile.toURI();

	    // create feature map for the transducer
	    FeatureMap transducerFeatureMap = Factory.newFeatureMap();
	    try {
		// set the grammar location
		transducerFeatureMap.put("grammarURL", japeURI.toURL());
		// set the grammar encoding
		transducerFeatureMap.put("encoding", "UTF-8");
	    } catch (MalformedURLException e) {
		System.out.println("Malformed URL of JAPE grammar");
		System.out.println(e.toString());
	    }

	    // create an instance of a JAPE Transducer processing resource
	    ProcessingResource japeTransducerPR = (ProcessingResource) Factory.createResource("gate.creole.Transducer", transducerFeatureMap);

	    // create corpus pipeline
	    annotationPipeline = (SerialAnalyserController) Factory.createResource("gate.creole.SerialAnalyserController");

	    // add the processing resources (modules) to the pipeline
	    annotationPipeline.add(documentResetPR);
	    annotationPipeline.add(tokenizerPR);
	    annotationPipeline.add(sentenceSplitterPR);
	    annotationPipeline.add(annieGazetteer);
	    annotationPipeline.add(japeTransducerPR);
	    Corpus corpus = Factory.newCorpus("");


	    int c = 0;
	    for (int i = 1; i <= 10; i++) {
		try {
		    org.jsoup.nodes.Document doc = Jsoup.connect("http://www.last.fm/user/" + str + "/tracks?view=compact&page=" + i).get();
		    
		    Elements paginator = doc.select("div.whittle-pagination");
		    if (paginator == null) {
			return doc.body().text();
			//break;
		    } else {
			Elements pages = paginator.select(".whittle-pagination-selected");
			Element page = pages.first();
			Integer in = new Integer(i);
			if (page == null || !page.text().contentEquals(in.toString())) {
			    return paginator.text()+"<br>"+page.text();
			   // break;
			}

		    }
		    Elements reviews = doc.select("table#deletablert");
		    FeatureMap docParams = Factory.newFeatureMap();
		    docParams.put(Document.DOCUMENT_STRING_CONTENT_PARAMETER_NAME, "<!DOCTYPE html><html><head></head><body>" + reviews.html() + "</body></html>");
		    docParams.put(Document.DOCUMENT_MIME_TYPE_PARAMETER_NAME,
			    "text/html");
		    Document document = (Document) Factory.createResource("gate.corpora.DocumentImpl", docParams);
		    c++;
		    corpus.add(document);
		} catch (org.jsoup.HttpStatusException ex) {
		    return "<p>User " + str + " does not exist!</p>";
		}
	    }

	    if (c < 1) {
		
		return "<p>User " + str + " has no favorite artists! 1</p>";
	    } else {

		// set the corpus to the pipeline
		annotationPipeline.setCorpus(corpus);

		//run the pipeline
		annotationPipeline.execute();

		// loop through the documents in the corpus
		Map<String, Integer> authors = new HashMap<String, Integer>();
		for (int i = 0; i < corpus.size(); i++) {

		    Document wdoc = corpus.get(i);

		    // get the default annotation set
		    AnnotationSet as_default = wdoc.getAnnotations();

		    FeatureMap featureMap = null;
		    // get all Token annotations

		    //System.out.println(authors.get(i));
		    AnnotationSet annSetTokens = as_default.get("Artist", featureMap);
		    authors = insertTokens(annSetTokens, wdoc, authors);

		    /* System.out.println("Companies:");
		     AnnotationSet annSetTokensCompany = as_default.get("Company", featureMap);
		     printTokens(annSetTokensCompany, doc);*/

		}
		SortedSet<Map.Entry<String, Integer>> sortedEntries = entriesSortedByValues(authors);
		String returnString = "";
		int j = 0;
		for (Entry<String, Integer> entry : sortedEntries) {
		    if (j == 0) {
			returnString += "<ul>"+str+"'s list of artists: ";
			j++;
		    }
		    String key = entry.getKey();
		    String value = entry.getValue().toString();
		    returnString += "<li>" + key + " listened to " + value + " times</li>";

		}
		if (j == 0) {
		    return "<p>User " + str + " has no favorite artists! 2</p>";
		} else {
		    returnString += "</ul>";
		}
		return returnString;
	    }

	} catch (GateException ex) {
	    Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
	    return ex.getMessage();
	}
	//return "<h2>Error 2</h2>";
    }

    private Map<String, Integer> insertTokens(AnnotationSet annSetTokens, Document doc, Map<String, Integer> authors) throws InvalidOffsetException {
	

	ArrayList tokenAnnotations = new ArrayList(annSetTokens);

	// looop through the Token annotations
	for (int j = 0; j < tokenAnnotations.size(); ++j) {

	    // get a token annotation
	    Annotation token = (Annotation) tokenAnnotations.get(j);

	    // get the underlying string for the Token
	    Node isaStart = token.getStartNode();
	    Node isaEnd = token.getEndNode();
	    String underlyingString = doc.getContent().getContent(isaStart.getOffset(), isaEnd.getOffset()).toString();
	    underlyingString = underlyingString.substring(0, underlyingString.length() - 2);

	    if (!authors.containsKey(underlyingString)) {
		authors.put(underlyingString, 1);
	    } else {
		int i = authors.get(underlyingString);
		authors.put(underlyingString, ++i);
	    }
	}
	return authors;
    }

    private void initialiseGate() {

	try {
	    // set GATE home folder
	    // Eg. /Applications/GATE_Developer_7.1
	    File gateHomeFile = new File("C:\\Program Files (x86)\\Gate");
	    Gate.setGateHome(gateHomeFile);

	    // set GATE plugins folder
	    // Eg. /Applications/GATE_Developer_7.1/plugins            
	    File pluginsHome = new File("C:\\Program Files (x86)\\Gate\\plugins");
	    Gate.setPluginsHome(pluginsHome);

	    // set user config file (optional)
	    // Eg. /Applications/GATE_Developer_7.1/user.xml
	    Gate.setUserConfigFile(new File("C:\\Program Files (x86)\\Gate", "user.xml"));

	    // initialise the GATE library
	    Gate.init();

	    // load ANNIE plugin
	    CreoleRegister register = Gate.getCreoleRegister();
	    URL annieHome = new File(pluginsHome, "ANNIE").toURL();
	    register.registerDirectories(annieHome);

	    // flag that GATE was successfuly initialised
	    isGateInitilised = true;

	} catch (MalformedURLException ex) {
	    Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
	} catch (GateException ex) {
	    Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
	}
    }

    SortedSet<Map.Entry<String, Integer>> entriesSortedByValues(Map<String, Integer> map) {
	SortedSet<Map.Entry<String, Integer>> sortedEntries = new TreeSet<Map.Entry<String, Integer>>(
		new Comparator<Map.Entry<String, Integer>>() {
	    @Override
	    public int compare(Map.Entry<String, Integer> e1, Map.Entry<String, Integer> e2) {
		int res = e1.getValue().compareTo(e2.getValue());
		if (res != 0) {
		    return (-1) * res;
		} else {
		    res = e1.getKey().compareToIgnoreCase(e2.getKey());
		    return res != 0 ? res : 1;
		}
	    }
	});
	sortedEntries.addAll(map.entrySet());
	return sortedEntries;
    }
}
