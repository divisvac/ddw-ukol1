/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ddw.ukol;

import java.io.IOException;
import java.net.MalformedURLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author MUDR
 */
public class LastFm extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	 String message = "<p>Enter LastFM username</p>";
        request.setAttribute("message", message); // This will be available as ${message}
        request.getRequestDispatcher("/index.jsp").forward(request, response);
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {
	
	String user = request.getParameter("user");
	String message = "<h2>Error 1</h2>";
	GateClient client = new GateClient();        
	try { 
	   message = client.run(user);
	} catch (MalformedURLException ex) {
	    message = "<h2>Error 3</h2>";
	} catch (IOException ex) {
	    message = "<h2>Error 4</h2>";
	}
        request.setAttribute("message", message); // This will be available as ${message}
        request.getRequestDispatcher("/index.jsp").forward(request, response);
    }
}
